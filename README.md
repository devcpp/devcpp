## 小龙（XiaoLoong）Dev-C++
小龙Dev-C++ （或者叫做 Dev-Cpp）是 Windows 环境下的一个轻量级 C/C++ 集成开发环境（IDE）。它是一款自由软件，遵守GPL许可协议分发源代码。它集合了功能强大的源码编辑器、MingW64/TDM-GCC 编译器、GDB 调试器和 AStyle 格式整理器等众多自由软件，适合于在教学中供 C/C++语言初学者使用，也适合于非商业级普通开发者使用。

#### 最新版本下载
小龙Dev-C++ 最新版本 v6.3（2024 年 12 月发布, 适用于 64 位 Windows 7/vista/8/10/11）下载：
- 下载链接1：[码云(Gitee)](https://gitee.com/devcpp/devcpp/releases/download/v6.3/Dev-Cpp-6.3-GCC-11.4.exe)
- 下载链接2：[开源共创(GitCode)](https://gitcode.com/devcpp/devcpp/releases/v6.3)
- 下载链接3：[Sourceforge](https://sourceforge.net/projects/devcpp20/)


也可以加入“小龙Dev-C++用户交流”QQ群（群号 311062360），从群文件中下载。

#### 开发历史
小龙Dev-C++是国外原有的 “Dev-C++”软件的中文分支版本。开发历史简介如下：
- Dev-C++ 的原始版本是 Colin Laplace 编写的 Bloodshed Dev-C++，该软件在 2005 年发布了 v4.9.9.2 后停止更新。
- Orwell (Johan Mes) 开发并维护了 Orwell Dev-C++，2016年发布了最终版本 v5.11之后停止更新。
- Embarcadero 公司在 2000-2021 年间资助了一些网友基于 Orwell Dev-C++ 5.11 进行改进开发，在 2021 年 1 月发布了 Embarcadero Dev-C++ 6.3 之后停止更新。
- 小龙Dev-C++ 是基于 Orwell Dev-C++ 5.11 进行改进开发的。2021-2023 年发布了 5.12 ~ 5.16，2024 年发布了 6.0 ~ 6.3。

#### 软件特色
- 重新设计了软件工作界面，主工具栏采用 32x32 大按钮，简洁、美观；
- 保存源文件时自动整理源代码缩进排版格式，使源代码变得整齐美观，符合行业标准；
- 集成了全中文的 GCC 编译器，编译时的输出信息全部显示为中文，方便阅读理解。
- 编译参数精心设置，能检查出大量常见错误（例如变量未初始化、输入输出格式不匹配、浮点数比较相等、变量遮蔽）；
- 调试时可以自动监视函数形参和局部变量的值，提高调试效率。


小龙Dev-C++ 针对编程初学者的实践需求，进行了精细化的设计，解决了编程教学中的一些常见困难：
- 支持单文件开发，初学者不需要建立项目就可以方便地进行程序编译和调试工作。
（VC6 必须要建立项目才能编译和调试，Code::Blocks 可以对单文件进行编译，但必须建立项目才能进行调试。）
- 提供了“插入代码块” 功能并自带了常见的代码块，用户可以在空白文件中直接插入“C Main”或 “C++ Main” ，然后开始编写自己的代码。
- 初学者通常很难按照规范的排版格式书写源代码，启用“保存文件时自动整理格式”选项，自动强制把源代码格式整理好。
- 初学者常常把半角英文字符写成全角字符，而精心设计语法高亮方案可以让这样的字符清楚可见，容易修改。
- 集成了 EGE 和 EasyX 图形函数库，方便计算机图形学课程的教学。


#### 其它版本下载 
旧版本： 5.16i（2022 年 10 月发布，适用于 64位 Windows 7/vista/8/10/11）：</p>
下载链接：[码云(Gitee)](https://gitee.com/devcpp/devcpp/releases/download/v5.16i/Dev-Cpp-5.16i.exe)

#### 软件架构
本软件中的主体架构是用 Delphi 开发的，集成了各种组件协同工作。
小龙Dev-C++ 的源代码以压缩包文件提供，文件名示例：devcpp-Source-abli.2024-1231-v6.3.zip** 

#### 其它说明
2024年5月，码云(gitee) 对个人开发者关闭了网页服务"Gitee Pages"，所以小龙Dev-C++ 原有的作者主页“https://devcpp.gitee.io” 失效了。

现有项目主页1：[https://gitee.com/devcpp/devcpp](https://gitee.com/devcpp/devcpp)

现有项目主页2：[https://gitcode.com/devcpp/devcpp](https://gitcode.com/devcpp/devcpp)

